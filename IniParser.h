#pragma once

#ifndef INIPARSER_H
#define INIPARSER_H

#include <iostream>
#include <fstream>
#include <cstdio>
#include <string>
#include <sstream>
#include <algorithm>
#include <map>
#include <cstring>
#include <locale>

#include "Exception.h"

using namespace std;

class IniParser {

private:

	ifstream file_for_read;
	map<string, map<string, string>> data;

public:

	IniParser(string file_name);

	~IniParser();

	int DefineType(const string str) const;

	void Initialize(const string file_name);

	bool IsHaveSection(const string section_name) const;

	bool IsHaveParam(const string section_name, const string param_name) const;

	void Parse(const string file_name);

	//int GetValueInt(const string section_name, const string param_name);

	//double GetValueDouble(const string section_name, const string param_name);

	//string GetValueString(const string section_name, const string param_name);

	template<class T>
	T GetValue(const string section_name, const string param_name) const;
};


template<class T>
T IniParser::GetValue(const string section_name, const string param_name) const {
	if (!(IsHaveSection(section_name) && (IsHaveParam(section_name, param_name))))
		throw SectionOrParamError("SectionOrParamError");
	
	/* switch (DefineType(data.at(section_name).at(param_name))) {
	case 0:
		return stoi(data.at(section_name).at(param_name));
	case 1:
		return stod(data.at(section_name).at(param_name));
	case 2:
		return data.at(section_name).at(param_name);
	} */

	std::stringstream s;
	s << data.find(section_name)->second.find(param_name)->second;
	T res;
	s >> res;
	return res;
};


#endif INIPARSER_H
