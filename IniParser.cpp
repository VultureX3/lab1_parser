#include "IniParser.h"

IniParser::IniParser(string file_name)
{
	Initialize(file_name);
};

IniParser::~IniParser() {};


void IniParser::Initialize(const string file_name) {
	file_for_read.open(file_name);
	if (!file_for_read.is_open())
		throw FileError("FileError");
	Parse(file_name);
};


bool IniParser::IsHaveSection(const string section_name) const {
	if (data.count(section_name) == 0)
		return false;
	return true;
};

bool IniParser::IsHaveParam(const string section_name, const string param_name) const {
	if (!IsHaveSection(section_name)) return false;
	if (data.at(section_name).count(param_name) == 0)
		return false;
	return true;
};

void IniParser::Parse(string filename) {
	ifstream file(filename);
	string str, param, section, value;
	while (getline(file_for_read, str)) {
		str.erase(remove_if(str.begin(), str.end(), isspace), str.end()); // holy internet
		if (str.empty()) continue;
		str += ';';
		for (int i = 0; i != str.size(); i++) {
			char symb = str[i];
			if (symb == ';') {
				break;
			}
			if (symb == ']')
				section = str.substr(1, i - 1);
			if (symb == '=') {
				param = str.substr(0, i);
				value = str.substr(i + 1, str.find(';') - i - 1);
				map <string, string> input;
				if (data.count(section) == 0) {
					data[section] = { { param, value } };
				}
				else {
					data[section][param] = value;
				}
			}
		}
	}
};


/* int IniParser::GetValueInt(const string section_name, const string param_name) {
	if (IsHaveSection(section_name) && (IsHaveParam(section_name, param_name))) {
		if (DefineType(data[section_name][param_name]) != 0)
			throw IntError("IntError");
		return stoi(data[section_name][param_name]);
	}
	else
		throw SectionOrParamError("SectionOrParamError");
};


double IniParser::GetValueDouble(const string section_name, const string param_name) {
	if (IsHaveSection(section_name) && (IsHaveParam(section_name, param_name))) {
		if (DefineType(data[section_name][param_name]) != 1)
			throw DoubleError("DoubleError");
		return stod(data[section_name][param_name]);
	}
	else
		throw SectionOrParamError("SectionOrParamError");
};

string IniParser::GetValueString(const string section_name, const string param_name) {
	if (IsHaveSection(section_name) && (IsHaveParam(section_name, param_name))) {
		if (DefineType(data[section_name][param_name]) != 2)
			throw StringError("StringError");
		return data[section_name][param_name];
	}
	else
		throw SectionOrParamError("SectionOrParamError");
}; */

int IniParser::DefineType(const string str) const {  // 2 - str, 1 - double, 0 - int
	int dots = 0;
	for (int i = 0; i != str.size(); i++) {
		if (!isdigit(str[i]) && str[i] != '.')
			return 2;
		if (str[i] == '.') dots++;
	}
	if (dots > 1 || dots == 1 && str.size() == 1) return 2;
	if (dots == 1) return 1;
	return 0;
};
