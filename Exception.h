#pragma once

#ifndef EXCEPTION_H
#define EXCEPTION_H

#include <exception>
#include <string>

class ParseException : public std::exception
{
public:
	ParseException(const std::string &message);
	~ParseException();


	const char* what() const;

private:
	std::string message;
};

class FileError : public ParseException
{
public:
	FileError(const std::string &message);
};

class SectionOrParamError : public ParseException
{
public:
	SectionOrParamError(const std::string &message);
};

class IntError : public ParseException
{
public:
	IntError(const std::string &message);
};

class DoubleError : public ParseException {
public:
	DoubleError(const std::string &message);
};

class StringError : public ParseException
{
public:
	StringError(const std::string &message);
};


// throw Exception("No such file");
// throw Exception("It's not integer");
// throw Exception("There's no such *section name* or *parameter name*");
// throw Exception("It's not double");
// throw Exception("It's not string");

#endif EXCEPTION_H
