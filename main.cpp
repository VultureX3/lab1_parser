#include <iostream>
#include <string>

#include "IniParser.h"

using namespace std;

int main()
{
	try {
		string section, filename, param;
		// char type;
		cout << "Enter filename: ";
		cin >> filename;
		IniParser file(filename);
		cout << "Enter your section: ";
		cin >> section;
		cout << "Enter your parameter: ";
		cin >> param;
		cout << file.GetValue<string> (section, param) << '\n';
		/* cout << "Enter parameter's type (i (int), d (double) or s (string)): ";
		cin >> type;
		switch (type) {
		case 'i':
			cout << file.GetValueInt(section, param) << '\n';
			break;
		case 'd':
			cout << file.GetValueDouble(section, param) << '\n';
			break;
		case 's':
			cout << file.GetValueString(section, param) << '\n';
			break;
		default:
			throw Exception("Invalid type");
		} */
		}
	catch (ParseException &e) {
		cout << e.what() << '\n';
	}
	system("pause");
}

