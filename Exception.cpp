#include "Exception.h"

ParseException::ParseException(const std::string &message) : message(message) { }

ParseException::~ParseException() { }

const char* ParseException::what() const
{
	return message.c_str();
}

FileError::FileError(const std::string &message) : ParseException(message) { }

SectionOrParamError::SectionOrParamError(const std::string &message) : ParseException(message) { }

IntError::IntError(const std::string &message) : ParseException(message) { }

DoubleError::DoubleError(const std::string &message) : ParseException(message) { }

StringError::StringError(const std::string &message) : ParseException(message) { }